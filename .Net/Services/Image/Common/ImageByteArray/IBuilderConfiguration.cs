﻿namespace CryptoCanvas.Services.Image.Common.ImageByteArray
{
    public interface IBuilderConfiguration
    {
        int GetWidthInPixels();
        int GetHeightInPixels();
    }
}
