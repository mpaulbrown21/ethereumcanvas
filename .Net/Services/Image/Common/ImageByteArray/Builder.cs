﻿using System.Drawing;
using System.IO;

namespace CryptoCanvas.Services.Image.Common.ImageByteArray
{
    public class Builder : IBuilder
    {
        IBuilderConfiguration _builderConfiguration;

        public Builder( IBuilderConfiguration builderConfiguration )
        {
            _builderConfiguration = builderConfiguration;
        }

        public byte[] Build( Pixel[] pixelArray )
        {
            Bitmap bitMap = BuildBitMap( pixelArray );

            return ImageToByteArray( bitMap );
        }

        private Bitmap BuildBitMap( Pixel[] pixelArray )
        {
            var bitmap = new Bitmap( _builderConfiguration.GetWidthInPixels(), _builderConfiguration.GetHeightInPixels() );

            for ( int x = 0; x < bitmap.Width; x++ )
            {
                for ( int y = 0; y < bitmap.Height; y++ )
                {
                    Pixel pixel = pixelArray[x * bitmap.Width + y];

                    bitmap.SetPixel( x, y, Color.FromArgb(pixel.Red, pixel.Green, pixel.Blue ) );
                }
            }

            return bitmap;
        }

        private static byte[] ImageToByteArray( Bitmap img )
        {
            using ( var stream = new MemoryStream() )
            {
                img.Save( stream, System.Drawing.Imaging.ImageFormat.Png );
                return stream.ToArray();
            }
        }
    }
}
