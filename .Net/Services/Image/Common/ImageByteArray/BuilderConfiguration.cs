﻿namespace CryptoCanvas.Services.Image.Common.ImageByteArray
{
    public class BuilderConfiguration : IBuilderConfiguration
    {
        public int GetWidthInPixels()
        {
            return Constants.CANVAS_PIXEL_WIDTH;
        }

        public int GetHeightInPixels()
        {
            return Constants.CANVAS_PIXEL_HEIGHT;
        }
    }
}
