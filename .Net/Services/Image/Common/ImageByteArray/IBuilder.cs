﻿namespace CryptoCanvas.Services.Image.Common.ImageByteArray
{
    public interface IBuilder
    {
        byte[] Build( Pixel[] pixelArray );
    }
}