﻿namespace CryptoCanvas.Services.Image.Common
{
    public static class Constants
    {
        public const int CANVAS_PIXEL_WIDTH = 10;
        public const int CANVAS_PIXEL_HEIGHT = 10;
    }
}
