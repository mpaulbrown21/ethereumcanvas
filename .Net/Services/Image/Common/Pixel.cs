﻿using System.Drawing;

namespace CryptoCanvas.Services.Image.Common
{
    public struct Pixel
    {
        public byte Red { get { return Color.R; } }
        public byte Green { get { return Color.G; } }
        public byte Blue { get { return Color.B; } }

        public Color Color { get; }

        public Pixel( byte red, byte green, byte blue )
        {
            Color = Color.FromArgb( red, green, blue );
        }

        public Pixel( int rgb )
        {
            Color = Color.FromArgb( rgb );
        }

        public int ToInt()
        {
            return ((Red & 0x0ff) << 16) | ((Green & 0x0ff) << 8) | (Blue & 0x0ff);
        }
    }
}
