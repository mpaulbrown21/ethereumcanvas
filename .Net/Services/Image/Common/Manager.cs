﻿using CryptoCanvas.Services.Image.CanvasRestApi;
using CryptoCanvas.Services.Image.ClientService;
using CryptoCanvas.Services.Image.Common.ImageByteArray;
using CryptoCanvas.Services.Image.Repository;
using System.Threading.Tasks;

namespace CryptoCanvas.Services.Image.Common
{
    public class Manager : IManager
    {
        private readonly IRepository _repository;
        private readonly IBuilder _imageByteArrayBuilder;

        public Manager( IRepository repository, IBuilder imageByteArrayBuilder )
        {
            _repository = repository;
            _imageByteArrayBuilder = imageByteArrayBuilder;
        }

        public async Task<byte[]> GetCanvasDataAsync()
        {
            Pixel[] pixelData = await _repository.GetPixelListAsync();

            return _imageByteArrayBuilder.Build( pixelData );
        }
        public async Task<bool> SetPixelAsync( int rgb, int x, int y )
        {
            Pixel pixel = new( rgb );
            CanvasIndex canvasIndex = new( x + ( y * Constants.CANVAS_PIXEL_HEIGHT ) + 1 );

            return await _repository.SetPixelAsync( pixel, canvasIndex.GetIndex() );
        }
    }
}
