﻿using Newtonsoft.Json;

namespace CryptoCanvas.Services.Image.Common.CanvasRestAPi
{
    public class CanvasRequestResponseModel 
    {
        [JsonProperty( "id" )]
        public string Id;

        [JsonProperty( "rgb" )]
        public string Rgb;
    }
}
