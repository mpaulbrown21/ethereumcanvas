﻿using CryptoCanvas.Shared.Framework.ApiClient;
using System.Threading.Tasks;

namespace CryptoCanvas.Services.Image.Common.CanvasRestApi
{
    public class CanvasRestApiClient : ICanvasRestApiClient
    {
        private readonly IApiClient _apiClient;

        public CanvasRestApiClient( IApiClient apiClient )
        {
            _apiClient = apiClient;
        }

        public async Task<System.IO.Stream> GetListAsync()
        {
            return await _apiClient.GetStreamAsync("canvas/");
        }

        public async Task<bool> Update( int rgb, int index )
        {
            SetPixelRequestModel data = new()
            {
                rgb = $"{rgb}",
                index = $"{index}",
                status = "U"
            };

            return ( await _apiClient.PostRequestAsync( "setpixelrequest/", data ) ).IsSuccessStatusCode;
        }
    }
}
