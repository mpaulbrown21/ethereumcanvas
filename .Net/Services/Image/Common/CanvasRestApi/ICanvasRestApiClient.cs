﻿using System.Threading.Tasks;
using System.IO;

namespace CryptoCanvas.Services.Image.Common.CanvasRestApi
{
    public interface ICanvasRestApiClient
    {
        Task<Stream> GetListAsync();

        Task<bool> Update( int rgb, int index );
    }
}