﻿namespace CryptoCanvas.Services.Image.Common.CanvasRestApi
{
    public interface ICanvasIndex
    {
        public int GetIndex();
    }
}