﻿using Newtonsoft.Json;

namespace CryptoCanvas.Services.Image.Common.CanvasRestApi
{
    public class SetPixelRequestModel
    {
        [JsonProperty( "index" )]
        public string index;

        [JsonProperty( "rgb" )]
        public string rgb;

        [JsonProperty( "status" )]
        public string status;
    }
}
