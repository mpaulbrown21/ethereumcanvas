﻿using CryptoCanvas.Services.Image.Common;
using CryptoCanvas.Services.Image.Common.CanvasRestApi;
using System;

namespace CryptoCanvas.Services.Image.CanvasRestApi
{
    public class CanvasIndex : ICanvasIndex
    {
        private readonly int _index;

        public CanvasIndex( int index )
        {
            Validate(index);
            _index = index;
        }

        private void Validate( int index )
        {
            if ( index >= 0 && index < Constants.CANVAS_PIXEL_HEIGHT * Constants.CANVAS_PIXEL_WIDTH )
            {
                return;
            }
            
            throw new IndexOutOfRangeException();
        }

        public int GetIndex()
        {
            return _index;
        }
    }
}
