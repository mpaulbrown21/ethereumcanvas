﻿using CryptoCanvas.Services.Image.Common.CanvasRestApi;
using CryptoCanvas.Services.Image.Common.CanvasRestAPi;
using CryptoCanvas.Services.Image.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;

namespace CryptoCanvas.Services.Image.Common
{
    public class Repository : IRepository
    {
        readonly ICanvasRestApiClient _canvasRestApiClient;

        public Repository( ICanvasRestApiClient canvasRestApiClient )
        {
            _canvasRestApiClient = canvasRestApiClient;            
        }

        public async Task<bool> SetPixelAsync( Pixel pixel, int index )
        {
            return await _canvasRestApiClient.Update( pixel.ToInt(), index );
        }

        public async Task<Pixel[]> GetPixelListAsync()
        {
            var pixelListStream = await _canvasRestApiClient.GetListAsync();

            using var streamReader = new StreamReader( pixelListStream );
            using var jsonTextReader = new JsonTextReader( streamReader );

            Pixel[] pixelArray = new Pixel[Constants.CANVAS_PIXEL_HEIGHT * Constants.CANVAS_PIXEL_WIDTH];

            while ( jsonTextReader.Read() )
            {
                if ( jsonTextReader.TokenType == JsonToken.StartObject )
                {
                    JObject jObject = JObject.Load( jsonTextReader );

                    var model = JsonConvert.DeserializeObject<CanvasRequestResponseModel>( jObject.ToString() );
                    Pixel pixel = new( int.Parse( model.Rgb ) );

                    pixelArray[int.Parse( model.Id ) - 1] = pixel;
                }
            }

            return pixelArray;
        }
    }
}
