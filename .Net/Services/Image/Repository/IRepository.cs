﻿using CryptoCanvas.Services.Image.Common;
using System.Threading.Tasks;

namespace CryptoCanvas.Services.Image.Repository
{
    public interface IRepository
    {
        Task<bool> SetPixelAsync( Pixel pixel, int index );

        Task<Pixel[]> GetPixelListAsync();
    }
}
