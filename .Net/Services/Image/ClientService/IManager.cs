﻿using System.Threading.Tasks;

namespace CryptoCanvas.Services.Image.ClientService
{
    public interface IManager
    {
        Task<byte[]> GetCanvasDataAsync();
        Task<bool> SetPixelAsync( int rgb, int x, int y );
    }
}
