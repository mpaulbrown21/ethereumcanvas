﻿using System.ComponentModel.DataAnnotations;

namespace CryptoCanvas.Services.Image.ClientService
{
    public class SetPixelModel
    {
        [Required]
        public int rgb;

        [Required]
        public int x;

        [Required]
        public int y;
    }
}
