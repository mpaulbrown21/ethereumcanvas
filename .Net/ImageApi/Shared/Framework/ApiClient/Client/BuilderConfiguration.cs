﻿namespace CryptoCanvas.Shared.Framework.ApiClient.Client
{
    public class BuilderConfiguration : IBuilderConfiguration
    {
        public string GetBaseAddress()
        {
            return Constants.BASE_ENDPOINT;
        }

        public string GetAuthToken()
        {
            return Constants.AUTH_TOKEN; 
        }
    }
}
