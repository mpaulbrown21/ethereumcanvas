﻿namespace CryptoCanvas.Shared.Framework.ApiClient.Client
{
    public interface IBuilderConfiguration
    {
        string GetBaseAddress();
        string GetAuthToken();
    }
}
