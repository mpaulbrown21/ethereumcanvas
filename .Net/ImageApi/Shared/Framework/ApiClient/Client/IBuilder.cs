﻿using System.Net.Http;

namespace CryptoCanvas.Shared.Framework.ApiClient.Client
{
    public interface IBuilder
    {
        HttpClient Build();
    }
}