﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CryptoCanvas.Shared.Framework.ApiClient.Client
{
    public class Builder : IBuilder
    {
        private readonly IBuilderConfiguration _configuration;

        public Builder( IBuilderConfiguration configuration )
        {
            _configuration = configuration;
        }

        public HttpClient Build()
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri( _configuration.GetBaseAddress() )
            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue( "Token", $"{_configuration.GetAuthToken()}" );
            client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/json" ) );

            return client;
        }
    }
}
