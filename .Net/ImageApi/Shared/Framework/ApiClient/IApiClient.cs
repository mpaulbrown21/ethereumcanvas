﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoCanvas.Shared.Framework.ApiClient
{
    public interface IApiClient
    {
        Task<HttpResponseMessage> GetRequestAsync( string requestString );
        Task<System.IO.Stream> GetStreamAsync( string requestString );
        Task<HttpResponseMessage> PostRequestAsync<T>( string requestString, T bodyData );
    }
}
