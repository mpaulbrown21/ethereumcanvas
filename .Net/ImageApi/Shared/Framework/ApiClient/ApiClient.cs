﻿using CryptoCanvas.Shared.Framework.ApiClient.Client;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CryptoCanvas.Shared.Framework.ApiClient
{
    public class ApiClient : IApiClient
    {
        private HttpClient _client;

        public ApiClient( IBuilder apiHttpClientBuilder )
        {
            _client = apiHttpClientBuilder.Build();
        }

        public async Task<HttpResponseMessage> GetRequestAsync( string requestString )
        {
            return await _client.GetAsync( requestString );
        }

        public async Task<System.IO.Stream> GetStreamAsync( string requestString )
        {
            return await _client.GetStreamAsync( requestString );
        }

        public async Task<HttpResponseMessage> PostRequestAsync<T>( string requestString, T bodyData )
        {
            var bodyDataSerialized = JsonConvert.SerializeObject( bodyData );
            var data = new StringContent( bodyDataSerialized, Encoding.UTF8, "application/json" );

            return await _client.PostAsync( requestString, data );
        }
    }
}
