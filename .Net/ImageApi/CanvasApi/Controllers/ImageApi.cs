﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Threading.Tasks;

namespace CryptoCanvas.ImageApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageApi : ControllerBase
    {
        private readonly ILogger<ImageApi> _logger;
        private readonly CryptoCanvas.Services.Image.ClientService.IManager _manager;

        public ImageApi( ILogger<ImageApi> logger, CryptoCanvas.Services.Image.ClientService.IManager manager )
        {
            _logger = logger;
            _manager = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var byteArray = await _manager.GetCanvasDataAsync();

            return new FileContentResult(byteArray, "image/png");
        }

        [HttpPost]
        public async Task<IActionResult> Set( int rgb, int x, int y )
        {
            var result = await _manager.SetPixelAsync( rgb, x, y );

            if ( result )
            {
                return StatusCode( (int)HttpStatusCode.OK );            
            }
            else
            {
                return StatusCode( (int)HttpStatusCode.InternalServerError );
            }
        }
    }
}
