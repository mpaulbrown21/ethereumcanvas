using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace CryptoCanvas.ImageApi 
{
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CanvasApi", Version = "v1" });
            });
            services.AddSingleton<Shared.Framework.ApiClient.Client.IBuilderConfiguration, Shared.Framework.ApiClient.Client.BuilderConfiguration>();
            services.AddSingleton<Shared.Framework.ApiClient.Client.IBuilder, Shared.Framework.ApiClient.Client.Builder>();
            services.AddSingleton<Shared.Framework.ApiClient.IApiClient, Shared.Framework.ApiClient.ApiClient>();
            services.AddSingleton<Services.Image.Common.CanvasRestApi.ICanvasRestApiClient, Services.Image.Common.CanvasRestApi.CanvasRestApiClient>();
            services.AddSingleton<Services.Image.Repository.IRepository, Services.Image.Common.Repository>();
            services.AddSingleton<Services.Image.Common.ImageByteArray.IBuilderConfiguration, Services.Image.Common.ImageByteArray.BuilderConfiguration>();
            services.AddSingleton<Services.Image.Common.ImageByteArray.IBuilder, Services.Image.Common.ImageByteArray.Builder>();
            services.AddSingleton<Services.Image.ClientService.IManager, Services.Image.Common.Manager>();

            services.AddCors( options =>
            {
                options.AddPolicy( name: "DevelopmentPolicy",
                                  builder =>
                                  {
                                      builder.WithOrigins( "http://localhost:24354", "https://localhost:44335" )
                                      .AllowAnyHeader()
                                      .AllowAnyMethod()
                                      .AllowCredentials();
                                  } );
            } );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CanvasApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("DevelopmentPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
