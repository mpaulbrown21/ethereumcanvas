﻿using CryptoCanvas.Canvas.Common.CanvasRestApi;
using CryptoCanvas.Services.Canvas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CryptoCanvas.Canvas.Common.Tests
{
    [TestClass]
    public class CanvasIndexTests
    {
        [TestMethod]
        public void CanvasConstructor_MinValidParameters_Success()
        {
            _ = new CanvasIndex( 0 );
        }

        [TestMethod]
        public void CanvasConstructor_MaxValidParameters_Success()
        {
            _ = new CanvasIndex( Constants.CANVAS_PIXEL_HEIGHT * Constants.CANVAS_PIXEL_WIDTH - 1 );
        }

        [ExpectedException( typeof( IndexOutOfRangeException ) )]
        [TestMethod]
        public void CanvasConstructor_NegativeIndex_ExceptionThrown()
        {
            _ = new CanvasIndex( -1 );
        }

        [ExpectedException( typeof( IndexOutOfRangeException ) )]
        [TestMethod]
        public void CanvasConstructor_IndexAboveMax_ExceptionThrown()
        {
            _ = new CanvasIndex( Constants.CANVAS_PIXEL_HEIGHT * Constants.CANVAS_PIXEL_WIDTH );
        }

        [TestMethod]
        public void Get_Index_Success()
        {
            const int index = 20;

            var testObject = new CanvasIndex( index );

            Assert.AreEqual( index, testObject.GetIndex() );
        }
    }
}