﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using CryptoCanvas.Shared.Framework.ApiClient;
using CryptoCanvas.Services.Image.Common.CanvasRestApi;
using CryptoCanvas.Services.Image.Common;

namespace CryptoCanvas.Shared.Framework.IntegrationTests
{
    [TestClass]
    public class CanvasRepositoryServiceTests 
    {
        [TestMethod]
        public async Task GetListAsync_ValidRequest_Success()
        {
            ApiClient.Client.BuilderConfiguration configuration = new ApiClient.Client.BuilderConfiguration();
            ApiClient.Client.Builder clientBuilder = new ApiClient.Client.Builder( configuration );
            IApiClient apiClient = new ApiClient.ApiClient( clientBuilder );
            CanvasRestApiClient client = new CanvasRestApiClient( apiClient );
            Repository repository = new Repository( client );

            var result = await repository.GetPixelListAsync();

            Assert.AreEqual( 
                Services.Image.Common.Constants.CANVAS_PIXEL_WIDTH * Services.Image.Common.Constants.CANVAS_PIXEL_HEIGHT, 
                result.Length 
            );
        }
    }
}
