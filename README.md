# EthereumCanvas

## About 
This project creates a private testable crypto currency called the "Crypto Canvas Token". The token can be traded or "burned" in order to set a pixel on a "Crypto Canvas". The "Crypto Canvas" is an image, containing 1 million pixels. The idea came from the r/place april fools day event.

The goal of this project is to perform smart contract transactions on a private ethereum network. It is used for me to learn how to interact with smart contracts. A smart contract is code that runs on the Ethereum Virtual Machine. The Ethereum Virtual Machine is essentially a global computer, where each computer that participates in blockchain validation also runs smart contract code.

This project incldues code for deploying an ERC-20 Token, the "Crypto Canvas Token", which gives the holder the capability to set one pixel in the "Crypto Canvas". In other words, they set a pixel in an image that everyone in the world could acquire. They can also modify it too by spending 1 Crypto Canvas Token (CCT).

There are four applications that work together in this software package. 

The lowest layer is an Ethereum Private Network with an ERC-20 Token deployed. The private Ethereum Network is monitored by a python script. Specifically, the python script monitors transactions on that network. When a transaction occurs, it sends a post request to the django rest api. The post request stores information about that transaction in a database. It does that because it is much easier to read from a database then it is to read from an ethereum network. 

A django rest api stores the results of the transactions on the ethereum network, so it is easy to request information on the latest updates to the canvas.

An Asp.net api communicates with the django rest api, and the blazor web assembly front-end. It requests pixel data from the django-rest-api, and builds an image to be served up to the browser.

A blazor web assembly front-end builds the UI for an end user to interact with the crypto-canvas. They can send transactions to the ethereum network by setting a pixel, and can view the resulting image.

For testing purposes, the end user who interacts with the UI will be spending tokens from the "test account". That way, they don't actually own any tokens. We can demonstrate how a user might interact with the token in the real world.

## Setup
Each of the four applications have an associated docker image for setup. All required configuration values must be passed into the docker build files. Each parameter is listed here for assitance.

### Ethereum Private Network

Build and run the docker image for the private ethereum network ( includes the python monitoring script )

`docker build --tag smartcontracttesting --build-arg EthereumNodeUrl=localhost:8545 .
`

`docker run -e CANVAS_API_ENDPOINT='http://localhost:8000/canvas/' -e SETPIXELREQUEST_API_ENDPOINT='http://localhost:8000/setpixelrequest/' -e AUTH_TOKEN='60397230059e718768ed4f4ab82a2bcc26c6bce4' smartcontracttesting`

NOTE: The auth token above should only be used for development purposes!

### Django Rest Api For DB Access
Currently working on building with docker compose file.

### Asp.net Api 
Docker Compose File Builds It. DOCUMENTATION NEEDED

### Blazor Web Project
Docker Compose File Builds It. DOCUMENTATION NEEDED
