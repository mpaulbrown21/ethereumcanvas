#!/usr/bin/python3
import brownie


def test_index_below_max(accounts, token):
    index = 99999
    green = '00ff00'

    token.setPixel(index, green)

    assert token.pixelRgbValue(index) == hex(int(green, 16))


def test_failed_transaction_index_above_max(accounts, token):
    index = 100000
    green = '00ff00'

    # Similar to pytest raises
    with brownie.reverts():
        token.setPixel(index, green)


def test_coin_burn(accounts, token):
    balance = token.balanceOf(accounts[0])

    index = 5
    red = 'ff0000'

    token.setPixel(index, red)

    assert token.balanceOf(accounts[0]) == balance - 1
