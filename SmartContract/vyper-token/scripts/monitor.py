import json
import threading
import os

import requests
from brownie import Token, accounts, network


def main():
    manager = NetworkManager()

    manager.monitor()


class SetPixelRequest:
    def __init__(self, rgb, index, status):
        self.id = id
        self.rgb = rgb
        self.index = index
        self.status = status


class NetworkManager:
    def __init__(self):

        self.canvas_api_endpoint = os.environ['CANVAS_API_ENDPOINT']
        self.setpixelrequest_api_endpoint = os.environ['SETPIXELREQUEST_API_ENDPOINT']

        environ_auth_token = os.environ['AUTH_TOKEN']
        self.auth_token = f'Token: {environ_auth_token}'
        self.api_authorization_headers = {
            'authorization': self.auth_token
        }
        self.deploy_token()
        print("Starting monitoring...")
        print(f"Canvas Api Endpoint: {self.canvas_api_endpoint}")
        print(f"Set Pixel Request Api Endpoint {self.setpixelrequest_api_endpoint}")
        print("Starting monitoring...")

    @staticmethod
    def deploy_token():
        print("Deploying token...")
        Token.deploy("CryptoCanvasToken", "TST", 18, 1e23, {'from': accounts[0]})
        print("Token deployed...")

    def monitor(self):
        self.monitor_task()
        threading.Timer(10, self.monitor).start()

    def monitor_task(self):
        set_pixel_requests = self.get_set_pixel_requests()

        for i in range(0, len(set_pixel_requests)):
            self.process_set_pixel_request(set_pixel_requests[i], i + 1)

    def process_set_pixel_request(self, set_pixel_request, set_pixel_request_id):
        if set_pixel_request.status == 'U':
            self.execute_transaction(set_pixel_request.index, set_pixel_request.rgb)
            self.update_canvas(set_pixel_request.index, set_pixel_request.rgb)
            self.report_transaction(set_pixel_request, set_pixel_request_id)

    def get_set_pixel_requests(self):
        response = requests.get(url=f'{self.setpixelrequest_api_endpoint}')

        print(f'Get Set Pixel Request Response {response.text}')
        return json.loads(response.text, object_hook=lambda d: self.SetPixelRequest(**d))

    def update_canvas(self, index, rgb):
        data = {
            "id": index,
            "rgb": rgb
        }

        url = f'{self.canvas_api_endpoint}{index}/'
        print(f"Attempting url: {url}")

        request = requests.put(
            url=url,
            data=data,
            headers=self.api_authorization_headers,
        )
        print(request)

    def report_transaction(self, set_pixel_request, set_pixel_request_id):
        data = {
            "index": set_pixel_request.index,
            "rgb": set_pixel_request.rgb,
            "status": 'A'
        }

        request = requests.put(
            url=f'{self.setpixelrequest_api_endpoint}{set_pixel_request_id}/',
            data=data,
            headers=self.api_authorization_headers,
        )
        print(request)

    @staticmethod
    def execute_transaction(index, rgb):
        Token[0].setPixel(index, rgb, {'from': accounts[0]})
