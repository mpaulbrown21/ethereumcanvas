#!/usr/bin/python3
# TODO: Resolve issue of PyCharm saying Token is not found once resolved on Gitter.
# TODO: Other developers have recently encountered the same issue.
# TODO: Issue doesn't stop code from being run. https://gitter.im/eth-brownie/community
from brownie import accounts, Token


def main():
    return Token.deploy("Test Token", "TST", 18, 1e21, {'from': accounts[0]})
