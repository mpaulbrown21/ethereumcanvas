# @version ^0.2.0

"""
@title Bare-bones Token implementation
@notice Based on the ERC-20 token standard as defined at
        https://eips.ethereum.org/EIPS/eip-20
"""

from vyper.interfaces import ERC20

implements: ERC20


event Approval:
    owner: indexed(address)
    spender: indexed(address)
    value: uint256

event Transfer:
    sender: indexed(address)
    receiver: indexed(address)
    value: uint256

event PixelSet:
    sender: indexed(address)
    index: uint256
    rgb: bytes32

name: public(String[64])
symbol: public(String[32])
decimals: public(uint256)
totalSupply: public(uint256)

balances: HashMap[address, uint256]
allowances: HashMap[address, HashMap[address, uint256]]

pixelData: public(bytes32[100000])


@external
def __init__(_name: String[64], _symbol: String[32], _decimals: uint256, _total_supply: uint256):
    self.name = _name
    self.symbol = _symbol
    self.decimals = _decimals
    self.balances[msg.sender] = _total_supply
    self.totalSupply = _total_supply
    log Transfer(ZERO_ADDRESS, msg.sender, _total_supply)


@view
@external
def balanceOf(_owner: address) -> uint256:
    """
    @notice Getter to check the current balance of an address
    @param _owner Address to query the balance of
    @return Token balance
    """
    return self.balances[_owner]


@view
@external
def allowance(_owner : address, _spender : address) -> uint256:
    """
    @notice Getter to check the amount of tokens that an owner allowed to a spender
    @param _owner The address which owns the funds
    @param _spender The address which will spend the funds
    @return The amount of tokens still available for the spender
    """
    return self.allowances[_owner][_spender]


@view
@external
def pixelRgbValue(_index: uint256) -> bytes32:
    """
    @notice Getter to retrieve a single pixel, at the given index
    @param _index The pixel index
    @return The rgb value of the pixel
    """
    return self.pixelData[_index]


@external
def approve(_spender : address, _value : uint256) -> bool:
    """
    @notice Approve an address to spend the specified amount of tokens on behalf of msg.sender
    @dev Beware that changing an allowance with this method brings the risk that someone may use both the old
         and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
         race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
         https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
    @param _spender The address which will spend the funds.
    @param _value The amount of tokens to be spent.
    @return Success boolean
    """
    self.allowances[msg.sender][_spender] = _value
    log Approval(msg.sender, _spender, _value)
    return True


@internal
def _transfer(_from: address, _to: address, _value: uint256):
    """
    @dev Internal shared logic for transfer and transferFrom
    """
    assert self.balances[_from] >= _value, "Insufficient balance"
    self.balances[_from] -= _value
    self.balances[_to] += _value
    log Transfer(_from, _to, _value)


@external
def transfer(_to : address, _value : uint256) -> bool:
    """
    @notice Transfer tokens to a specified address
    @dev Vyper does not allow underflows, so attempting to transfer more
         tokens than an account has will revert
    @param _to The address to transfer to
    @param _value The amount to be transferred
    @return Success boolean
    """
    self._transfer(msg.sender, _to, _value)
    return True


@external
def transferFrom(_from : address, _to : address, _value : uint256) -> bool:
    """
    @notice Transfer tokens from one address to another
    @dev Vyper does not allow underflows, so attempting to transfer more
         tokens than an account has will revert
    @param _from The address which you want to send tokens from
    @param _to The address which you want to transfer to
    @param _value The amount of tokens to be transferred
    @return Success boolean
    """
    assert self.allowances[_from][msg.sender] >= _value, "Insufficient allowance"
    self.allowances[_from][msg.sender] -= _value
    self._transfer(_from, _to, _value)
    return True


@internal
def _burnSingleCoin(_from : address):
    """
    @notice We want to make sure that the zero address is not
    @used. Otherwise, a threat actor could set the total supply to
    @0 by constantly calling any method that calls burn.
    @param _from The address which you want to burn a token from
    """
    assert _from != ZERO_ADDRESS
    self._transfer(_from, ZERO_ADDRESS, 1)
    log Transfer(_from, ZERO_ADDRESS, 1)


@internal
def _setPixel(_from: address, _index: uint256, _rgb : bytes32):
    """
    @notice Set the pixel at the given index, to the 
    rgb value. Only up to index 100000 is valid.
    @param _from The address setting a pixel
    @param _index The index to set a pixel at
    @param _rgb The rgb value to set the pixel to.
    """
    assert self.balances[_from] >= 1, "Insufficient balance"
    self._burnSingleCoin(_from)
    self.pixelData[_index] = _rgb
    log PixelSet(_from, _index, _rgb)


@external
def setPixel(_index: uint256, _rgb : bytes32):
    """
    @notice Set the pixel at the given index, to the rgb value
    @param _index The index to set a pixel at
    @param _rgb The rgb value to set the pixel to.
    """
    self._setPixel(msg.sender, _index, _rgb) 


@external
def setPixelFrom(_from : address, _index: uint256, _rgb : bytes32): 
    """
    @notice Set the pixel at the given index, to the rgb value.
    @param _index The index to set a pixel at
    @param _rgb The rgb value to set the pixel to.
    """
    assert self.allowances[_from][msg.sender] >= 1, "Insufficient allowance"
    self.allowances[_from][msg.sender] -= 1
    self._setPixel(_from, _index, _rgb)
