from rest_framework import viewsets, permissions
from rest_framework.authentication import TokenAuthentication

from api import constants
from api.models import Canvas, SetPixelRequest
from api.serializers import CanvasSerializer, SetPixelRequestSerializer


class CanvasViewSet(viewsets.ModelViewSet):
    """
    View set for providing `list`, `create`, `retrieve`,
    `update` and `destroy` for the canvas.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Canvas.objects.filter(id__lte=constants.PIXEL_CAP)
    serializer_class = CanvasSerializer


class SetPixelRequestViewSet(viewsets.ModelViewSet):
    """
    View set for providing `list`, `create`, `retrieve`,
    `update` and `destroy` for the canvas.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = SetPixelRequest.objects.all()
    serializer_class = SetPixelRequestSerializer
