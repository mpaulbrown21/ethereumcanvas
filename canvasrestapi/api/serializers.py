from rest_framework import serializers

from api.models import Canvas, SetPixelRequest


class CanvasSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Canvas
        fields = ['id', 'rgb']


class SetPixelRequestSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SetPixelRequest
        fields = ['index', 'rgb', 'status']
