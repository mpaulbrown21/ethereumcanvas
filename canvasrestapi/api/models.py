from django.core.validators import MaxValueValidator
from django.db import models

from api import constants


class Canvas(models.Model):
    id = models.IntegerField(primary_key=True)
    rgb = models.IntegerField(validators=[MaxValueValidator(constants.MAX_RGB_INTEGER)])


class SetPixelRequest(models.Model):
    ACCEPTED = 'A'
    REJECTED = 'R'
    UNPROCESSED = 'U'
    STATUS_CHOICES = [
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected'),
        (UNPROCESSED, 'Unprocessed'),
    ]
    index = models.IntegerField(validators=[MaxValueValidator(constants.MAX_RGB_INTEGER)])
    rgb = models.IntegerField(validators=[MaxValueValidator(constants.PIXELS_IN_CANVAS)])
    status = models.CharField(choices=STATUS_CHOICES, max_length=1, default=UNPROCESSED)
